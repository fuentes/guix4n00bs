# Comment écrire une recette Guix pour les débutants

## Introduction

Guix comme gestionnaire de paquet est vraiment pratique pour gérer les piles logicielles
complexes. Comme il est transactionnel, on peut à tout moment revenir à un etat precedent
identique (essayez avec dpkg ou spack, c'est juste impossible). De plus,
son système de composition des paquets garantit une reproduction bit à bit
des binaires pour une même architecture, ce qui est un plus dans un contexte d'experimentation
scientifique. Ainsi, installer des paquets comme MUMPS ou Maphys, devient un jeu d'enfant
comparé au calvaire parfois rencontré lors de l'utilisation des modules Tcl/Tk.
De plus, le langage d'écriture (Guile) des recettes Guix étant fonctionnel, ceci assure
une bonne robustesse au processus de création des paquets. Cependant, ceci peut rendre la marche 
d'entrée plutôt élevée en apparence tout du moins. C'est pourquoi j'ai voulu ici
partager les quelques astuces et résultats glanés ici et là.

## Préréquis

- Avoir installé Guix sur sa machine : on peut bosser sur la frontale d'un serveur comme Plafrim
mais si la compilation du paquet échoue, on n'aura pas accès aux artefacts de compilation (option `--keep-failed`)

Pour installer Guix en tant que gestionnaire de paquet en local, on peut utiliser la méthode suivante : 

```bash
cd /tmp
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
chmod +x guix-install.sh
sudo ./guix-install.sh
```

Puis mettez en place les variables d'environnement suivantes dans votre `.bash_profile` ou `.profile`
```bash
export PATH="$HOME/.guix-profile/bin${PATH:+:}$PATH"
export GUIX_PROFILE="$HOME/.config/guix/current"
export PATH="$GUIX_PROFILE/bin${PATH:+:}$PATH"
```


Si on souhaite aussi profiter des paquets orientés HPC developpés à l'Inria, on pourra copier coller dans son
`~/.config/guix/channels.scm` les lignes suivantes

```scheme
(cons*
  (channel
    (name 'guix-science)
    (url "https://github.com/guix-science/guix-science")
    (introduction
      (make-channel-introduction
        "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
        (openpgp-fingerprint
          "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))))
  (channel
    (name 'guix-hpc)
    (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git"))

  (channel
    (name 'guix-hpc-non-free)
    (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git"))
  %default-channels)
```
qui permetteront de disposer des canaux de développement de recettes Guix à l'INRIA, `guix-hpc-non-free` et `guix-hpc`, ainsi
que d'un canal dedié aux applications scientifiques

## Exemple d'intro

Supposons que l'on ait un logiciel avec comme unique dépendance MPI. On considerera que
la méthode de compilation est donnée dans un fichier `CMakeLists.txt`. On prendra comme exemple,
le sempiternel code de la chaleur [heat](https://gitlab.inria.fr/sed-bso/heat).
L'écriture d'une recette d'un paquet *Guix* consiste à écrire un fichier source `.scm` (en Guile) qui 
va décrire les phases habituelles d'installation d'un paquet :

 - téléchargement
 - decompression (`unpack`)
 - application éventuelle de rustines (les fameux patches d'outre-manche)
 - compilation (`build`)
 - installation et copies des fichiers dans le dossier cible (out) → phase (`install`)
 - et plein d'autres que l'on n'a pas detaillé (tests, doc, etc...)

Que l'on utilise `CMake` ou le bon vieux `./configure && make && make install` de Gnu, Guix
fournit des implémentations de base pour chaque phase du processus. Le codeur d'une recette
Guix va donc devoir remplacer ou amender certaines phases pour que le gestionnaire de paquets
fasse ce qu'il desire. 

### Squelette de démarrage

On a crée dans le dépôt le paquet suivant `bouts_de_code/zutos.scm`, afin de compiler le code de la chaleur

```scheme
(define-module (bouts_de_code zutos)
      #:use-module (guix)
      #:use-module (guix git-download)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages mpi))
      
(define-public zutos
 (package
  (name "zutos")
  (version "0.01")
  (source
     (origin
       (method url-fetch)
       (uri "https://gitlab.inria.fr/sed-bso/heat/-/archive/v1.0.0/heat-v1.0.0.tar.gz")
       (sha256 (base32 "05j6l87xphxygzn61fnqvqcp5186jfirij2pbzbq5nhkjjlcddk3"))
       ))
  (build-system cmake-build-system)
  (synopsis "résoudre l'équation de la chaleur")
  (native-inputs `(("openmpi" ,openmpi)))                     
  (description "une description succinte")
  (license #f)
  (home-page "https://gitlab.inria.fr/sed-bso/heat")))
```

Nous allons juste après décrire chacun des ingrédients de base que contient la recette, mais pour les impatients
vous pouvez déjà tester la compilation avec
```bash
cd guix4n00bs
guix build -KL . zutos
```
le KL n'est pas là pour battre des records, mais résulte de la combinaison des options de compil :
- `--keep-failed` pour garder les produits de la compilation si cette dernière a échoué 
- `--load_path=.` pour utiliser le répertoire courant comme chemin de recherche du paquet à compiler. 

On verra très bientot comment les utiliser pour déverminer.

### structure d'un paquet
  
une recette est donc la construction d'un objet(enregistrement en pascal, type derivé en fortran, 
structure en C) a partir des des differentes composantes champs.  Par exemple

```scheme
(define-public zutos (package....))
```
define publiquement (exposé meme a l'exterieur du module), le résultat de l'application de la fonction `package` a tous 
ses arguments, dont on peut donner une liste (non-exhaustive), mais minimale :

* name : une chaîne de caractères pour le nom
* version : une chaîne pour la version
* source : un argument décrivant la méthode pour télécharger l'archive ou l'arborescence Git
* build-system : une valeur décrivant le systeme de compilation 
  * `cmake-build-system` pour CMake 
  * `gnu-build-system` pour GNU autotools
  * `copy-file-system` pour copier une arborescence (pratique avec des sources sous forme d'entête)
* synopsis : une chaîne de caractères décrivant succintement ce que fait le paquet
* inputs (optionel) : un argument pour décrire les dépendances, on reviendra la dessus plus tard
* description : une chaîne de caractères - souvent sur plusieurs lignes - pour décrire le paquet
* license : un argument décrivant la licence ; on peut utiliser #f (faux en Guile) pour ne pas mettre de licence
* home-page : une chaîne de caractères qui indique la page d'acceuil du projet

Remarques :
* l'ordre des arguments est complétement interchangeable. 
* toute modif dans le fichier qui ne produirait pas un changement de résultat de la compil (en l'occurence
la permutation des arguments) ne va pas induire de nouveau processus d'installation et le résultat sera 
toujours dans le [dépot](https://guix.gnu.org/manual/fr/html_node/Le-depot.html#Le-d_00e9p_00f4t) au moins endroit
avec la même clef de hachage

Exercices : 
- supprimer la ligne contenant native-inputs et lancer guix `guix build -KL . zutos` ? Que se passe-t-il et pourquoi?

### Téléchargement

On va décrire une ou deux méthodes ici pour télécharger un projet. En fait il faut voir quelles valeurs peut on donner
à manger la fonction  `source`

#### Cas d'une archive
Si le code est dispo sur le ouaib, on peut utiliser la méthode suivante

```scheme
(source
   (origin
     (method url-fetch)
     (uri "https://url_archive.tgz")
     (sha256 (base32 "clef_de_hachage"))
     ))
```

Comment obtenir la clef de hachage : Par exemple, 

```bash
mkdir -p /tmp/cono && cd /tmp/cono
wget https://gitlab.inria.fr/sed-bso/heat/-/archive/v1.0.0/heat-v1.0.0.tar.gz
guix download file://$PWD/heat-v1.0.0.tar.gz
```
renvoie bien la clef de hachage précédente "05j6l87xphxygzn61fnqvqcp5186jfirij2pbzbq5nhkjjlcddk3"

## Amender, remplacer ou supprimer une phase du processus de constuction

  On peut amender une phase en utilisant `add-before` ou `add-after` sur l'argument optionel #:phases
```scheme
(arguments 
`(#:phases
  (modify-phases %standard-phases
         ((add-before 'build 'fix-date-error
            (lambda _
              (setenv "SOURCE_DATE_EPOCH" "315532800"))) 
     ))))
```
_Note_: on utilise une astuce classique utile l'installation de certains paquets Python qui permet de modifier de l'estampille temporelle.
mais on peut aussi supprimer grâce à  `delete` ou remplacer avec `replace` comme dans
```scheme
(replace 'check (lambda _ #t)))))
```

## Modifier les sources

  Il a de nombreuses façons de modifier les sources avant l'etape de configuration : nous allons en décrire trois

### appliquer une rustine
  
   Dans le champ `source` on peut rajouter une composante `patches` comme
```scheme
(patch-flags '("-p0"))
(patches (list  (local-file "vtk-geos-cmake-fix.patch"))
```
l'option de niveau d'application des rustines est par defaut `-p1`, mais on peut la changer
grâce a l'argument `patch-flags`

### changer une expression dans un fichier

 la fonction `substitute\*` permet de chercher une expression rationnelle dans un fichier et de la modifier
```
(add-before 'configure 'change-idx-type
	      (lambda _
                (substitute* "metis/include/metis.h"
                  (("define IDXTYPEWIDTH 32") "define IDXTYPEWIDTH 64"))))
```
permet de changer directement une chaine de caractère dans un fichier. 

### utiliser une bribe de code

 la composante *origin* d'un paquet contient un champ *snippet* , ce dernier peut contenir en effet
 du code préparatoire afin de modifier le code avant les étapes de configuration et de compilation.

## liste des licences

 voir (la)[https://git.savannah.gnu.org/cgit/guix.git/tree/guix/licenses.scm]


### Idiotismes guilesques
 Pour comprendre et écrire des recettes Guix, la principale difficulté provient de l'apprentissage du langage `Guile` qui n'est pas du plus simple abord. 

### Apostrophes et accent grave.

  Lorsque l'on commence à lire des recettes Guix, on peut être heurté au premier abord par des expressions singulières du genre
```scheme
(native-inputs `(("openmpi" ,openmpi)))                     
```
Première remarque, la virgule n'EST PAS un séparateur, c'est un raccourci pour la fonction `unquote`, elle s'applique donc à l'élèment jusqu'au prochain espace, ou au
groupe parenthésé, lorsque elle est suivie de parenthèses. L'opérateur «quasi-dual» de la virgule, c'est l'accent grave qui est un raccourci pour la fonction `quasiquote`
. Comme Guile est de la famille des langages Lisp, il est homo-iconique, c'est à dire que le code peut être traité comme une donnée et ouvre donc d'énormes
possibilités de faire de la métaprogrammation. Comme certains recettes vont être interprétées par le démon Guix lors de la construction des recettes,

«\`» permet de ne pas évaluer une expression, mais si `,` est utilisé sur certains elements alors ceux ci seront evalués

```scheme
(define x 2)
(define y 3)
`(1 ,x ,y) ;; → (1 2 3)
`(1 ,x y) ;; → (1 2 y)
```

#### déverminer à l'affichage

```scheme
  (display "mon_texte_a_afficher")
```
permet de deverminer et de tester de menus tâches. On recupère ainsi dans la console certaines valeurs

#### comment écrire dans un fichier

```scheme
(call-with-output-file "hehe" (lambda (port) (format port "truc")))
```
permet d'ecrire la chaine "truc" dans le fichier  "hehe". la chaîne peut «interpoler» des variables en utilisant le caractère special `~a`

####  fonctions anonymes 
- on peut utiliser l'instruction `lambda` (mot-clef que l'on retrouve en Python, au grand desespoir de Guido Van Rossum 😉)

```scheme
((lambda (x) (* 2 x)) 3) ;; → 6
```
- une autre façon idiomatique : après avoir chargé le module srfi-26, on peut definir des `lambda` plus facilement en «découpant» - d'ou le nom `cut` - 
l'endroit où l'on va mettre la variable d'entrée de la fonction anonyme avec un trou marqué par le digramme `<>`, ce qui donne par exemple
avec la fonction `*`
```scheme
(use-modules (srfi srfi-26))
((cut * 2 <>) 3) ;; → 6
```
Attention toutefois, si vous utilisez ces modules pour des phases modifiées de la compilation, certaines parties ne sont evaluées que lors de l'appel
de la recette et l'import des modules est nécessaire localement à la recette
```scheme
`(
  #:modules ((ice-9 ftw)
             (srfi srfi-1)
             (srfi srfi-26)
             (guix build utils)
             (guix build cmake-build-system))
   ...
)
```
permettra de s'en sortir.

### Découvrir la façon de coder des recettes

- Une fonctionnalité super pratique `guix edit <mon_paquet>` permet de voir comment sont codés la plupart des recettes :  par exemple,

```scheme
guix edit python-ansicolors
```
ouvre votre éditeur preferé sur la recette du paquet `python-ansicolors`

```scheme
(define-public python-ansicolors
  (package
    (name "python-ansicolors")
    (version "1.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ansicolors" version ".zip"))
       (sha256
        (base32 "1q3jqglkq4z0f6nkkn8bswcwqg012i2grrc27kabr8286dg4zycr"))))
    (build-system python-build-system)
    (native-inputs
     (list python-tox
           python-pytest-cov
           unzip))
    (home-page "https://github.com/jonathaneunice/colors/")
    (synopsis "ANSI colors for Python")
    (description
     "This package adds ANSI colors and decorations to your strings.")
    (license license:isc)))

- l'autre méthode pour découvrir des exemples d'utilisation de fonction de l'API consiste a charge les sources de guix et a utiliser git grep

```
### importer un paquet pypi

  Le système de paquet Pypi a une méthode standard d'installation que l'on peut utiliser pour generer automatiquement une trame pour la recette Guix
  desirée `guix import pypi <mon_paquet>`. par exemple

```bash
guix import pypi plotille
```
nous génère la recette suivante :

```scheme
(package
  (name "python-plotille")
  (version "5.0.0")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "plotille" version))
            (sha256
             (base32
              "1p5k5bh2rx0hvy4lb501yyrs9af6q8y8dc53sgm25jg4l98wmrcr"))))
  (build-system python-build-system)
  (home-page "https://github.com/tammoippen/plotille")
  (synopsis "Plot in the terminal using braille dots.")
  (description "Plot in the terminal using braille dots.")
  (license license:expat))%
```

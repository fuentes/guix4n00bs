(define-module (bouts_de_code zutos)
      #:use-module (guix)
      #:use-module (guix git-download)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages mpi))
      
(define-public zutos
 (package
  (name "zutos")
  (version "0.01")
  (source
     (origin
       (method url-fetch)
       (uri "https://gitlab.inria.fr/sed-bso/heat/-/archive/v1.0.0/heat-v1.0.0.tar.gz")
       (sha256 (base32 "05j6l87xphxygzn61fnqvqcp5186jfirij2pbzbq5nhkjjlcddk3"))
       ))
  (build-system cmake-build-system)
  (synopsis "résoudre l'équation de la chaleur")
  (native-inputs `(("openmpi" ,openmpi)))                     
  (description "une description succinte")
  (license #f)
  (home-page "https://gitlab.inria.fr/sed-bso/heat")))

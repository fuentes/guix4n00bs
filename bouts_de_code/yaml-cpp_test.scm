(define-module (bouts_de_code yaml-cpp_test)
      #:use-module (guix)
      #:use-module (guix licenses)
      #:use-module (guix git-download)
      #:use-module (guix build-system cmake)
      #:use-module (gnu packages gcc)
      #:use-module (gnu packages maths)
      #:use-module (gnu packages commencement)
      #:use-module (gnu packages python))

(define-public yaml-cpp_new
  (package
    (name "yaml-cpp_new")
    (version "0.6.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jbeder/yaml-cpp")
             (commit (string-append "yaml-cpp-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ykkxzxcwwiv8l8r697gyqh1nl582krpvi7m7l6b40ijnk4pw30s"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DYAML_BUILD_SHARED_LIBS=ON")))
    (native-inputs
      `(("python" , python-3)
        ("gcc-toolchain" ,gcc-toolchain-8)))

    (home-page "https://github.com/jbeder/yaml-cpp")
    (synopsis "YAML parser and emitter in C++")
    (description "YAML parser and emitter in C++ matching the YAML 1.2 spec.")
    (license bsd-3)))

